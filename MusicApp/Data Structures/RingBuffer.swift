//
//  RingBuffer.swift
//  guitarTuner
//
//  Created by Alejandro Castillejo on 7/15/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation

public struct RingBuffer<T> {
    
    fileprivate var array: [T?]
    fileprivate var writeIndex = 0
    
    public init(count: Int) {
        array = [T?](repeating: nil, count: count)
    }
    
    public mutating func write(_ element: T) {
        array[writeIndex % array.count] = element
        writeIndex += 1
        
    }
    
    public mutating func readBuffer() -> [T]? {
        
        var fullArray: [T] = []
        
        for element in array {
            if element != nil {
                fullArray.append(element!)
            } else {
                return nil
            }
        }
        
        return fullArray
    }
    
}
