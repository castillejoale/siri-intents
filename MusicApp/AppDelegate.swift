//
//  AppDelegate.swift
//  MusicApp
//
//  Created by Alejandro Castillejo on 6/30/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import UIKit
import Intents

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    // This method is called when the application is background launched in response to the extension returning .handleInApp.
    func application(_ application: UIApplication, handle intent: INIntent, completionHandler: @escaping (INIntentResponse) -> Void) {
        
        print("App delegate intent handle")

        guard let playMediaIntent = intent as? INPlayMediaIntent else {
            completionHandler(INPlayMediaIntentResponse(code: .failure, userActivity: nil))
            return
        }
        
        handlePlayMediaIntent(playMediaIntent, completion: completionHandler)
    }
    
    func handlePlayMediaIntent(_ intent: INPlayMediaIntent, completion: @escaping (INPlayMediaIntentResponse) -> Void) {
        
        print("App delegate handlePlayMediaIntent")

        guard let mediaItem = intent.mediaItems?.first, let _ = mediaItem.identifier else {
            return
        }

        if mediaItem.title?.lowercased()  == "my melody" {
            PlayerController.shared.play()
        } else if mediaItem.title?.lowercased() == "a e note" || mediaItem.title?.lowercased() == "e" || mediaItem.title?.lowercased() == "e note" {
            let mp3 = SiriMP3("pianoHighE", rate: 1.0)
            SiriSimulatorController.shared.play(mp3)
        }

    }
    
    func application(_ application: UIApplication, willContinueUserActivityWithType userActivityType: String) -> Bool {
        return true
    }
    
}
