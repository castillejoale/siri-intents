//
//  SiriEvent.swift
//  MusicApp
//
//  Created by Alejandro Castillejo on 7/16/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation

enum SiriEvent: String {
    
    case loopCreated = "loopCreated"
    case loopConfirmation = "loopConfirmation"
    
    case melodyRecorded = "melodyRecorded"
    case melodyConfirmation = "melodyConfirmation"
    case melodyConfirmed = "melodyConfirmed"
    //    case melodyPlayed = "melodyPlayed"
    case recording = "recording"
    
    case chordRecognition = "chordRecognition"
    case noteRecognition = "noteRecognition"
    
    case highE = "highE"
    case lowE = "lowE"
    
    case tuneGuitar = "tuneGuitar"
    case tuningConfirmed = "tuningConfirmed"
    case needTune = "needTune"
    
    case unknown = "unknown"
    
}
