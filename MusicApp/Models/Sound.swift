//
//  Sound.swift
//  guitarTuner
//
//  Created by Alejandro Castillejo on 7/15/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation

struct Sound {
    
    let pitch: Pitch
    let amplitude: Double
    let distance: Double
    
}
