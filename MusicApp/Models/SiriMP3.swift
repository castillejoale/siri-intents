//
//  SiriMP3.swift
//  MusicApp
//
//  Created by Alejandro Castillejo on 7/14/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation

class SiriMP3: SiriAudio {
    
    let fileName: String
    let rate: Float
    
    required init(_ fileName: String, rate: Float) {
        self.fileName = fileName
        self.rate = rate
    }
    
}
