//
//  Utterance.swift
//  MusicApp
//
//  Created by Alejandro Castillejo on 7/5/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation

class SiriUtterance: SiriAudio {
    
    let string: String
    
    required init(_ id: String, string: String) {
        self.string = string
    }
    
}
