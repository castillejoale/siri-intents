//
//  SiriSound.swift
//  MusicApp
//
//  Created by Alejandro Castillejo on 7/5/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation

enum SiriSoundType: String {
    
    case end = "siriEnd"
    case error = "siriError"
    case start = "siriStart"
    
}

class SiriSound: SiriAudio {

    let type: SiriSoundType

    required init(_ type: SiriSoundType) {
        self.type = type
    }

}

//class SiriSound: SiriAudio {
//    
//    var event: SiriEvent
//    let type: SiriSoundType
//
//    required init(_ type: SiriSoundType, event: SiriAudioEvent) {
//        self.type = type
//        self.event = event
//    }
//
//
//}
