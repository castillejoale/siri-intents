//
//  ChordRecognizer.swift
//  soundRecognizer
//
//  Created by Alejandro Castillejo on 7/20/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation
import CoreML
import AVFoundation
import SoundAnalysis

class ChordRecognizerController {
    
    static let shared = ChordRecognizerController()
    
    let soundClassifier = AleModel()
    var inputFormat: AVAudioFormat!
    var audioEngine = AVAudioEngine()
    var streamAnalyzer: SNAudioStreamAnalyzer!
    var resultsObserver =  ResultsObserver()
    // Serial dispatch queue used to analyze incoming audio buffers.
    let analysisQueue = DispatchQueue(label: "com.apple.AnalysisQueue")
    
    func setup() {
        
        resultsObserver.delegate = SiriSimulatorController.shared
        inputFormat = audioEngine.inputNode.inputFormat(forBus: 0)
        streamAnalyzer = SNAudioStreamAnalyzer(format: inputFormat)
        startCapturingAudio()
        
    }
    
    private func startCapturingAudio() {
        do {
            let request = try SNClassifySoundRequest(mlModel: soundClassifier.model)
            try streamAnalyzer.add(request, withObserver: resultsObserver)
        } catch {
            print("Unable to prepare request: \(error.localizedDescription)")
            return
        }
        
        audioEngine.inputNode.installTap(onBus: 0,
                                         bufferSize: 8192, // 8k buffer
        format: inputFormat) { buffer, time in
            
            let channelDataValue = buffer.floatChannelData!.pointee
            let channelDataValueArray = stride(from: 0,
                                               to: Int(buffer.frameLength),
                                               by: buffer.stride).map{ channelDataValue[$0] }
            
            self.analysisQueue.async {
                self.streamAnalyzer.analyze(buffer, atAudioFramePosition: time.sampleTime)
            }
            
        }
        
//        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            print("Failed to start your engine")
        }
        
    }
    
    func stop() {
        
        print("stop chord recognizer")
        
        audioEngine.stop()
        audioEngine.inputNode.removeTap(onBus: 0)
        
    }
    
}

protocol ResultsObserverDelegate {
    func determinedChordType(identifier: String, confidence: Double)
}

// Observer object that is called as analysis results are found.
class ResultsObserver : NSObject, SNResultsObserving {
    
    var delegate: ResultsObserverDelegate?
    
    func request(_ request: SNRequest, didProduce result: SNResult) {
        
        // Get the top classification.
        guard let result = result as? SNClassificationResult,
            let classification = result.classifications.first else { return }
        
        // Determine the time of this result.
        let formattedTime = String(format: "%.2f", result.timeRange.start.seconds)
//        print("Analysis result for audio at time: \(formattedTime)")
        
        let confidence = classification.confidence * 100.0
        let percent = String(format: "%.2f%%", confidence)
        
        // Print the result as Instrument: percentage confidence.
//        print("\(classification.identifier): \(percent) confidence.")
        self.delegate?.determinedChordType(identifier: classification.identifier, confidence: confidence)
    }
    
    func request(_ request: SNRequest, didFailWithError error: Error) {
        print("The the analysis failed: \(error.localizedDescription)")
    }
    
    func requestDidComplete(_ request: SNRequest) {
        print("The request completed successfully!")
    }
}
