//
//  STTController.swift
//  MusicApp
//
//  Created by Alejandro Castillejo on 7/5/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation
import Speech

protocol STTControllerProtocol {
    
    func transcriptionUpdated(_ string: String)
    func transcriptionFinished()
    
}

class STTController {
    
    static let shared = STTController()
    
    var delegate: STTControllerProtocol?
    
    let audioEngine = AVAudioEngine()
    let speechRecognizer = SFSpeechRecognizer()
    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?
    
    func setup() {
        
        SFSpeechRecognizer.requestAuthorization {
            [unowned self] (authStatus) in
//            switch authStatus {
//            case .authorized:
//                print("Speech recognition authorization successful")
//            case .denied:
//                print("Speech recognition authorization denied")
//            case .restricted:
//                print("Not available on this device")
//            case .notDetermined:
//                print("Not determined")
//            @unknown default:
//                print("ERROR")
//            }
        }
        
    }
    
    func startTranscription(){
        
        print("startTranscription")
        
        do{
            // 1
            let recordingFormat = audioEngine.inputNode.inputFormat(forBus: 0)
            print(recordingFormat)
            
            // 2
            audioEngine.inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { [unowned self] (buffer, _) in
                self.request.append(buffer)
            }
            
            
            // 3
            audioEngine.prepare()
            try audioEngine.start()
            
            
            //4
            recognitionTask = speechRecognizer?.recognitionTask(with: request) { [unowned self] (result, _) in
                
                if let transcription = result?.bestTranscription {
                    self.delegate?.transcriptionUpdated(transcription.formattedString)
                }
                
            }
        } catch {}
        
    }
    
    func stopTranscription() {
        
        print("stopTranscription")
        
        audioEngine.stop()
        request.endAudio()
        recognitionTask?.cancel()
        audioEngine.inputNode.removeTap(onBus: 0)
        
    }

}
