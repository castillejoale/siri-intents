//
//  BLEController.swift
//  MusicApp
//
//  Created by Alejandro Castillejo on 7/14/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation
import CoreBluetooth

let targetDeviceName = "Adafruit Bluefruit LE"

protocol BLEControllerProtocol {
    
//    func connected()
    func receivedMessage(string: String)
    
}

class BLEController: NSObject {
    
    var sampleCharacterisctic: CBCharacteristic?
    
    static let sharedInstance = BLEController()
    
    var delegate: BLEControllerProtocol?
    
    var centralManager: CBCentralManager?
    var peripheralOfInterest: CBPeripheral?
    
    func start() {
        
        // Create a concurrent background queue for the central
        let centralQueue: DispatchQueue = DispatchQueue(label: "com.iosbrain.centralQueueName", attributes: .concurrent)
        centralManager = CBCentralManager(delegate: self, queue: centralQueue)
        
    }
    
    func sendMessage() {
        
        let messageText = "Hello feather"
        let data = messageText.data(using: .utf8)
        peripheralOfInterest?.writeValue(data!, for: sampleCharacterisctic!, type: CBCharacteristicWriteType.withResponse)
        
    }
    
    func decodePeripheralState(peripheralState: CBPeripheralState) {
        
        switch peripheralState {
        case .disconnected:
//            print("Peripheral state: disconnected")
            break
        case .connected:
            print("Peripheral state: connected")
        case .connecting:
            print("Peripheral state: connecting")
        case .disconnecting:
            print("Peripheral state: disconnecting")
        @unknown default:
            print("Unknown peripheral state")
        }
        
    }
    
}

extension BLEController: CBCentralManagerDelegate {
    
    // MARK: - CBCentralManagerDelegate methods
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        switch central.state {

        case .unknown:
            print("Bluetooth status is UNKNOWN")
        case .resetting:
            print("Bluetooth status is RESETTING")
        case .unsupported:
            print("Bluetooth status is UNSUPPORTED")
        case .unauthorized:
            print("Bluetooth status is UNAUTHORIZED")
        case .poweredOff:
            print("Bluetooth status is POWERED OFF")
        case .poweredOn:
            print("Bluetooth status is POWERED ON")
            centralManager?.scanForPeripherals(withServices: nil, options: nil)
            
        @unknown default:
            print("Unknown central state")
        }
        
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        decodePeripheralState(peripheralState: peripheral.state)
        peripheralOfInterest = peripheral
        
        if let _ = advertisementData["kCBAdvDataServiceUUIDs"] as? NSArray {
            
            //            print(serviceUUIDs)
            
            if let deviceName = advertisementData["kCBAdvDataLocalName"] as? String {
                
//                print(deviceName)
                
                if deviceName == targetDeviceName {
                    // Stop scanning to preserve battery life
                    centralManager?.stopScan()
                    // Connect
                    centralManager?.cancelPeripheralConnection(peripheralOfInterest!)
                    centralManager?.connect(peripheralOfInterest!)
                }
                
            }
            
        }
        
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        print("Central connected to peripheral")
        
        peripheralOfInterest?.delegate = self
        
//        DispatchQueue.main.async { () -> Void in
//            // Update views
//            self.delegate?.connected()
//            
//        }
        
        if ((peripheralOfInterest?.services) != nil) {
            self.peripheral(peripheralOfInterest!, didDiscoverServices: nil)
        } else {
            peripheral.discoverServices(nil)
        }
        
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        print("Disconnected")
        centralManager?.scanForPeripherals(withServices: nil, options: nil)
        
    }
    
}

extension BLEController: CBPeripheralDelegate {
    
    // MARK: - CBPeripheralDelegate methods
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        //        print("Peripheral services")
        //        print(peripheral.services)
        
        for service in peripheral.services! {
            
//            print("Service: \(service)")
            
            // Look for characteristics of interest within services of interest
            peripheral.discoverCharacteristics(nil, for: service)
            
        }
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        for characteristic in service.characteristics! {
            
//            print(characteristic)
            
            if characteristic.properties.contains(.read) {
//                print("\(characteristic.uuid): properties contains .read")
                peripheral.readValue(for: characteristic)
            }
            if characteristic.properties.contains(.notify) {
//                print("\(characteristic.uuid): properties contains .notify")
                peripheral.setNotifyValue(true, for: characteristic)
            }
            
            sampleCharacterisctic = characteristic
            
        }
        
    }
    
    // We're notified whenever a characteristic value updates regularly or posts once; read and decipher the characteristic value(s) that we've subscribed to
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        if let data = characteristic.value {
            let str = String(decoding: data, as: UTF8.self)
            DispatchQueue.main.async { () -> Void in
                self.delegate?.receivedMessage(string: str)
            }
        }
        
    }
    
}
