//
//  PlayerController.swift
//  MusicApp
//
//  Created by Alejandro Castillejo on 7/4/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation
import AVFoundation

protocol PlayerControllerProtocol {
    func startedPlaying()
    func finishedPlayingRecording(success: Bool)
    func finishedPlayingSound(siriSound: SiriSound)
}

class PlayerController: NSObject, AVAudioPlayerDelegate {
    
    static let shared = PlayerController()
    var delegate: PlayerControllerProtocol?
    var player:AVAudioPlayer!
    
    func setup() {

        let fileURL = RecorderController.shared.getRecordedFilePath()
        
        do {
            player = try AVAudioPlayer(contentsOf: fileURL)
            player.delegate = self
            player.prepareToPlay()
            player.volume = 1.0
        } catch {
            print("AVAudioPlayer error")
        }

    }
    
    func play() {
        setup()
        player.play()
        delegate?.startedPlaying()
    }
    
    func stop() {
        player.stop()
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
        if flag {
            print("Audio sound player finished playing")
//            print(player.url?.absoluteString)
            if let pathArray = player.url?.absoluteString.split(separator: "/") {
                if let last = pathArray.last{
                    if let siriSoundType = SiriSoundType(rawValue: String(last).stripFileExtension()) {
                        let siriSound = SiriSound(siriSoundType)
                        self.delegate?.finishedPlayingSound(siriSound: siriSound)
                    } else {
                        delegate?.finishedPlayingRecording(success: flag)
                    }
                }
            }
            
        }
        
    }
    
    private func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer!, error: NSError!) {
        print("Error while playing audio \(error.localizedDescription)")
    }
    
    func playSound(_ sound: SiriSound) {
        
        guard let url = Bundle.main.url(forResource: sound.type.rawValue, withExtension: "mp3") else { return }
        
        do {
            
            try AVAudioSession.sharedInstance().setCategory(.playAndRecord, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            player?.delegate = self
            player?.volume = 1.0
            
            guard let player = player else { return }
            
            player.play()
            delegate?.startedPlaying()
            
        } catch let error {
            print(error.localizedDescription)
        }
        
    }
    
    func playMP3(_ fileName: String, rate: Float) {
        
        guard let url = Bundle.main.url(forResource: fileName, withExtension: "mp3") else { return }
        
        do {
            
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            guard let player = player else { return }
            
            player.delegate = self
            player.enableRate = true
            player.rate = rate
            
            player.play()
            delegate?.startedPlaying()
            
        } catch let error {
            print(error.localizedDescription)
        }
        
    }

}
