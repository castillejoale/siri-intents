//
//  SiriSimulatorController.swift
//  MusicApp
//
//  Created by Alejandro Castillejo on 7/14/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation

protocol SiriSimulatorControllerProtocol {
    func transcriptionUpdated(_ string: String)
    func transcriptionFinished()
}

class SiriSimulatorController {
    
    static let shared = SiriSimulatorController()
    var delegate: SiriSimulatorControllerProtocol?
    
    let silenceGap = 1
    
    var siriBusy = false
    
    var lastTranscriptionUpdate: Date?
    var lastTranscriptionString: String?
    
    var lastEvent: SiriEvent?
    
    var guitarStringsDistances: [Pitch: Double] = resetGuitarStrings()
    
    let bleQueue = DispatchQueue(label: "BLEQueue")
    var lastPressedButton = Date()
    
    var lastTuningNoteDate = Date()
    
    func setup() {
        
        BLEController.sharedInstance.delegate = self
        
        bleQueue.async {
            BLEController.sharedInstance.start()
        }
        
        PlayerController.shared.delegate = self
        
        RecorderController.shared.delegate = self
        RecorderController.shared.setup()
        
        STTController.shared.delegate = self
        
        TTSController.shared.delegate = self
        
    }
    
    func simulatedGuitarButtonPressed() {
        
        let secondsDifference = Calendar.current.dateComponents([.second], from: Date(), to: lastPressedButton).second ?? 0
        if abs(secondsDifference) <= 3 { return }
        
        lastPressedButton = Date()
        
        print("simulatedGuitarButtonPressed")
        
        if !siriBusy {
            startSiri()
        } else {
            finishTranscription()
        }
        
    }
    
    func startSiri() {
        
        print("startSiri")
        
        if !siriBusy {
            let siriSound = SiriSound(.start)
            play(siriSound)
            siriBusy = true
        }
        
    }
    
    func endSiri() {
        
        print("endSiri")
        
        if siriBusy {
            let siriSound = SiriSound(.end)
            play(siriSound)
            siriBusy = false
            self.delegate?.transcriptionFinished()
        }
    }
    
    func play(_ audio: SiriAudio) {
        
        if let utterance = audio as? SiriUtterance {
            TTSController.shared.playUtterance(utterance)
//            lastEvent = SiriEvent(rawValue: utterance.string)
        } else if let sound = audio as? SiriSound {
            PlayerController.shared.playSound(sound)
        } else if let mp3 = audio as? SiriMP3 {
            PlayerController.shared.playMP3(mp3.fileName, rate: mp3.rate)
//            lastEvent = SiriEvent(rawValue: mp3.fileName)
        }
        
    }
    
    func startGuitarTuning() {
        
        print("startGuitarTuning")
        
//        lastEvent = .tuneGuitar
        guitarStringsDistances = SiriSimulatorController.resetGuitarStrings()
        NoteRecognizerController.shared = NoteRecognizerController(.tuningGuitar)
        NoteRecognizerController.shared?.delegate = self
        NoteRecognizerController.shared?.startMonitoring()
    }
    
    func stopGuitarTuning() {
        NoteRecognizerController.shared?.delegate = nil
        NoteRecognizerController.shared?.stopMonitoring()
        NoteRecognizerController.shared = nil
    }
    
    func startTuningNote() {
        print("startNoteTuning")
        
        NoteRecognizerController.shared = NoteRecognizerController(.tuningNote)
        NoteRecognizerController.shared?.delegate = self
        NoteRecognizerController.shared?.startMonitoring()
        
    }
    
    static func resetGuitarStrings() -> [Pitch:Double] {
        return [
            Pitch(note: Note.e(nil), octave: 2):0.0,
            Pitch(note: Note.a(nil), octave: 2):0.0,
            Pitch(note: Note.d(nil), octave: 3):0.0,
            Pitch(note: Note.g(nil), octave: 3):0.0,
            Pitch(note: Note.b(nil), octave: 3):0.0,
            Pitch(note: Note.e(nil), octave: 4):0.0,
        ]
    }
    
    func startNoteRecognition() {
        print("startNoteRecognition")
        NoteRecognizerController.shared = NoteRecognizerController(.recognition)
        NoteRecognizerController.shared?.delegate = self
        NoteRecognizerController.shared?.startMonitoring()
    }
    
    func stopNoteRecognition() {
        print("stopNoteRecognition")
        NoteRecognizerController.shared?.stopMonitoring()
        NoteRecognizerController.shared?.delegate = nil
        NoteRecognizerController.shared = nil
        lastEvent = nil
    }
    
}

extension SiriSimulatorController: BLEControllerProtocol {
    
    func receivedMessage(string: String) {
        if string == "bp" {
            self.simulatedGuitarButtonPressed()
        }
    }
    
}

extension SiriSimulatorController: NoteRecognizerControllerProtocol {
    
    func noteRecognized(_ sound: Sound, mode: NoteRecognizerMode) {
        
        print(sound.pitch)
        print(sound.distance)
        
        if mode == .recognition {
            
            self.stopNoteRecognition()
            
            let string = "Its a \(sound.pitch.note.noteDescription) \(sound.pitch.note.accidentalDescription ?? "") note"
            
            let utterance = SiriUtterance("noteRecognized", string: string)
            play(utterance)
            
        } else if mode == .tuningGuitar {
            
            switch sound.pitch.description {
            case "E2", "A2", "D3", "G3", "B3", "E4": guitarStringsDistances[sound.pitch] = sound.distance
            default:
                print("not a string note")
            }
            
            if sound.pitch.description == "E4" {
                
                self.stopNoteRecognition()

                var array: [(Pitch, Double)] = []
                
                let threshold = 1.5
                
                for key in guitarStringsDistances.keys {
                    if abs(guitarStringsDistances[key]!) > threshold {
                        array.append((key, guitarStringsDistances[key]!))
                    }
                }
                
                if array.count == 0 {
                    let utterance = SiriUtterance("guitarTuning", string: "Your guitar is tuned")
                    play(utterance)
                    lastEvent = nil
                } else {
                    let tutple = array.last!
                    let utterance = SiriUtterance("guitarTuning", string: "The \(tutple.0.description) string is not tuned, want to tune it?")
                    lastEvent = .needTune
                    play(utterance)
                }

            }
            
            
        }  else if mode == .tuningNote {
            
            let secondsDifference = Calendar.current.dateComponents([.second], from: Date(), to: lastTuningNoteDate).second ?? 0
            if abs(secondsDifference) <= 1 { return }
            
            let threshold = 1.5
            
            if sound.distance > threshold {
                print("Down")
                let utterance = SiriUtterance("guitarTuning", string: "Down")
                play(utterance)
            } else if sound.distance < -threshold {
                print("Up")
                let utterance = SiriUtterance("guitarTuning", string: "Up")
                play(utterance)
            } else {
                self.stopNoteRecognition()
                print("String tuned")
                let utterance = SiriUtterance("guitarTuning", string: "Your guitar is tuned")
                play(utterance)
                lastEvent = nil
            }
            
            lastTuningNoteDate = Date()
            
        }
        
    }
    
}


extension SiriSimulatorController: PlayerControllerProtocol {
    
    func startedPlaying() {
         siriBusy = true
    }
    
    func finishedPlayingRecording(success: Bool) {
        
        siriBusy = false
        SiriSimulatorController.shared.endSiri()

        //Here
//        recorderButton.isEnabled = true
//        playerButton.setTitle("Play", for: .normal)
        
    }
    
    func finishedPlayingSound(siriSound: SiriSound) {
        
        if siriSound.type == SiriSoundType.end {

            if lastEvent == .loopCreated {
                let utterance = SiriUtterance("loopConfirmation", string: "Summer Jam loop saved, want me to play it now?")
                play(utterance)
            } else if lastEvent == .loopConfirmation {
                let mp3 = SiriMP3("loop", rate: 1.0)
                SiriSimulatorController.shared.play(mp3)
            } else if lastEvent == .melodyRecorded{
                let utterance = SiriUtterance("melodyConfirmation", string: "\(lastTranscriptionString!) saved, want me to play it now?")
                lastEvent = .melodyConfirmation
                play(utterance)
            } else if lastEvent == .melodyConfirmed {
                PlayerController.shared.play()
                lastEvent = .unknown
            } else if lastEvent == .recording {
                let utterance = SiriUtterance("melodyRecorded", string: "Melody recorded, what do you want to call it?")
                lastEvent = .melodyRecorded
                play(utterance)
            } else if lastEvent == .lowE {
                let mp3 = SiriMP3("guitarLowE", rate: 1.0)
                SiriSimulatorController.shared.play(mp3)
            } else if lastEvent == .highE {
                let mp3 = SiriMP3("guitarHighE", rate: 1.0)
                SiriSimulatorController.shared.play(mp3)
            } else if lastEvent == .tuneGuitar {
                startGuitarTuning()
            } else if lastEvent == .chordRecognition {
                ChordRecognizerController.shared.setup()
            }  else if lastEvent == .tuningConfirmed {
                SiriSimulatorController.shared.startTuningNote()
            }
            
//
            
        } else if siriSound.type == SiriSoundType.start {
            STTController.shared.startTranscription()
        }
        
    }
    
}

extension SiriSimulatorController: RecorderControllerProtocol {
    
    func startedRecording() {
        
        DispatchQueue.main.async {
            //Here
//            self.recorderButton.setTitle("Tap to Stop", for: .normal)
            self.siriBusy = true
            STTController.shared.startTranscription()
        }
        
    }
    
    func finishedRecording(success: Bool) {
        
        //Speech
        STTController.shared.stopTranscription()
        
        if success {
            //Here
//            recorderButton.setTitle("Tap to Record", for: .normal)
//            playerButton.isEnabled = true
            lastEvent = .recording
//            SiriSimulatorController.shared.endSiri()
            
        } else {
            //Here
//            recorderButton.setTitle("Tap to Record", for: .normal)
        }
        
    }
    
}

extension SiriSimulatorController: STTControllerProtocol {
    
    
    func transcriptionFinished() {
        print("transcriptionFinished")
    }
    
    func transcriptionUpdated(_ string: String) {
        
        lastTranscriptionString = string
        lastTranscriptionUpdate = Date()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + Double(self.silenceGap)) {
            
            if self.siriBusy == false { return }
            if self.lastEvent == SiriEvent.recording { return }
            guard let date = self.lastTranscriptionUpdate else { return }
            
            let secondsDifference = Calendar.current.dateComponents([.second], from: Date(), to: date).second ?? 0

            if abs(secondsDifference) >= self.silenceGap {
                self.finishTranscription()
            }
            
        }
        
        self.delegate?.transcriptionUpdated(string)
        
        if string.lowercased() == "what note is this" {
            if lastEvent != .noteRecognition {
                lastEvent = .noteRecognition
                startNoteRecognition()
            }
        } else if string.lowercased() == "recognize this" {
            lastEvent = .chordRecognition
            finishTranscription()
        } else if string.lowercased() == "on guitar" {
            lastEvent = .highE
            finishTranscription()
        } else if string.lowercased() == "one octave down" {
            lastEvent = .lowE
            finishTranscription()
        } else if string.lowercased() == "is my guitar tuned" || string.lowercased() == "tune guitar" {
            lastEvent = .tuneGuitar
            finishTranscription()
        }
        
        let wordsArray = string.split(separator: " ")
//        print("Words recognized \(wordsArray)")
        for word in wordsArray {
               
            if word.lowercased() == "melody" {
                finishTranscription()
            } else if word.lowercased() == "jam"  {
                finishTranscription()
            } else if word.lowercased() == "yes"  {
                
                if lastEvent == .needTune {
                    lastEvent = .tuningConfirmed
                    finishTranscription()
                } else if lastEvent == .melodyConfirmation {
                    lastEvent = .melodyConfirmed
                    finishTranscription()
                } else if lastEvent == .loopCreated {
                    lastEvent = .loopConfirmation
                    finishTranscription()
                }
            } else if word.lowercased() == "again"  {
                lastEvent = .lowE
                finishTranscription()
            }

        }
        
    }
    
    func finishTranscription() {
//        print("finishTranscription")
        STTController.shared.stopTranscription()
        RecorderController.shared.finishRecording(success: true)
        endSiri()
    }
    
}

extension SiriSimulatorController: TTSControllerProtocol {
    
    func finishedPlayingTTS() {
        print(lastEvent)
        
        if lastEvent == .melodyRecorded || lastEvent == .melodyConfirmation || lastEvent == .needTune || lastEvent == .loopCreated {
            startSiri()
        }
    }
    
}

extension SiriSimulatorController: ResultsObserverDelegate {
    
    func determinedChordType(identifier: String, confidence: Double) {
        if confidence > 98 && identifier != "Background" {
            print("\(identifier): \(confidence) confidence.")
            ChordRecognizerController.shared.stop()
            
            var string = ""
            
            if identifier == "Am" {
                string = "Its an A minor chord"
            } else if identifier == "G" {
                string = "Its a G chord"
            } else if identifier == "C" {
                string = "Its a C chord"
            } else if identifier == "F" {
                string = "Its a F chord"
            }
            
            let utterance = SiriUtterance("noteRecognized", string: string)
            play(utterance)
            
        }
    }
    
}
