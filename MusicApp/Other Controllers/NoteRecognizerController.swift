//
//  TunerController.swift
//  guitarTuner
//
//  Created by Alejandro Castillejo on 7/15/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation
import AudioKit

enum NoteRecognizerMode {
    case tuningGuitar
    case tuningNote
    case recognition
}

protocol NoteRecognizerControllerProtocol {
    func noteRecognized(_ sound: Sound, mode: NoteRecognizerMode)
}

class NoteRecognizerController: NSObject {
    
    static var shared:NoteRecognizerController?
    
    var delegate: NoteRecognizerControllerProtocol?
    
    var buffer: RingBuffer<Sound>!
    
    var amplitudeThreshold: Double = 0.00
    
    var lastClearSound:Sound?
    
    let mode: NoteRecognizerMode
    
    /* Private instance variables. */
    fileprivate var timer:      Timer?
    fileprivate let microphone: AKMicrophone
    fileprivate let analyzer:   AKAudioAnalyzer
    
    init(_ mode: NoteRecognizerMode) {
        /* Start application-wide microphone recording. */
        AKManager.shared().enableAudioInput()
        
        /* Add the built-in microphone. */
        microphone = AKMicrophone()
        AKOrchestra.add(microphone)
        
        /* Add an analyzer and store it in an instance variable. */
        analyzer = AKAudioAnalyzer(input: microphone.output)
        AKOrchestra.add(analyzer)
        
        if mode == .tuningGuitar || mode == .tuningNote {
            amplitudeThreshold = 0.04
            buffer = RingBuffer<Sound>(count: 7)
        } else if mode == .recognition {
            amplitudeThreshold = 0.10
            buffer = RingBuffer<Sound>(count: 3)
        }
        
        self.mode = mode
        
    }
    
    func startMonitoring() {
        
        print("startMonitoring")
        
        /* Start the microphone and analyzer. */
        analyzer.play()
        microphone.play()
        
        /* Initialize and schedule a new run loop timer. */
        timer = Timer.scheduledTimer(timeInterval: 0.05, target: self,
                                     selector: #selector(NoteRecognizerController.tick),
                                     userInfo: nil,
                                     repeats: true)
        
    }
    
    func stopMonitoring() {
        print("stopMonitoring")
        timer?.invalidate()
        analyzer.stop()
        microphone.stop()
    }
    
    @objc func tick() {
        /* Read frequency and amplitude from the analyzer. */
        

        let frequency = Double(analyzer.trackedFrequency.floatValue)
        let amplitude = Double(analyzer.trackedAmplitude.floatValue)
        
        /* Find nearest pitch. */
        let pitch = Pitch.nearest(frequency)
        
        /* Calculate the distance. */
        let distance = frequency - pitch.frequency
        
        let sound = Sound(pitch: pitch, amplitude: amplitude, distance: distance)
        
        buffer.write(sound)
        
        if let sound = clearSound() {
            lastClearSound = sound
            print("lastClearSound set")
            self.delegate?.noteRecognized(sound, mode: mode)
        }

    }
    
    func clearSound() -> Sound? {
        
        guard let sounds = buffer.readBuffer() else { return nil }
        
        var lastPitch:Pitch?
        
        for sound in sounds {
            
            if sound.amplitude < amplitudeThreshold {
                lastClearSound = nil
                return nil
            }
            
            if lastPitch != nil {
                if sound.pitch != lastPitch! {
                    lastClearSound = nil
                    return nil
                }
            } else {
                lastPitch = sound.pitch
            }
            
        }
        
        if mode != .tuningNote {
            if let lastClearPitch = lastClearSound?.pitch {
                if lastPitch! == lastClearPitch {
                    return nil
                }
            }
        }
        
        var amplitudeSum: Double = 0
        var distanceSum: Double = 0
        for sound in sounds {
            //            print(sound.distance)
            //            print(sound.amplitude)
            amplitudeSum = amplitudeSum + sound.amplitude
            distanceSum = distanceSum + sound.distance
        }
        let amplitudeAvg = amplitudeSum / Double(sounds.count)
        let distanceAvg = distanceSum / Double(sounds.count)
        
//        print(lastClearSound?.pitch)
//        print(distanceAvg)
        
        return Sound(pitch: lastPitch!, amplitude: amplitudeAvg, distance: distanceAvg)
        
    }
    
}
