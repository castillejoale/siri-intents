//
//  TTSController.swift
//  MusicApp
//
//  Created by Alejandro Castillejo on 7/5/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation
import AVFoundation

protocol TTSControllerProtocol {
    func finishedPlayingTTS()
}

class TTSController: NSObject, AVSpeechSynthesizerDelegate {
    
    static let shared = TTSController()
    var delegate: TTSControllerProtocol?
    
    let synthesizer = AVSpeechSynthesizer()

    func playUtterance(_ utterance: SiriUtterance) {
        
        synthesizer.delegate = self
        
        let voices = AVSpeechSynthesisVoice.speechVoices()
        
        var voice:AVSpeechSynthesisVoice
        
        //Get Siri's voice
        if #available(iOS 13, *) {
            voice = voices[17]
        } else {
            voice = voices[8]
        }
        
        let speechUtterance = AVSpeechUtterance(string: utterance.string)
        speechUtterance.voice = voice
        speechUtterance.rate = 0.5
        
        synthesizer.speak(speechUtterance)
        
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        self.delegate?.finishedPlayingTTS()
    }
    
}
