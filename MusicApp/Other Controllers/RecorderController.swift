//
//  RecorderController.swift
//  MusicApp
//
//  Created by Alejandro Castillejo on 7/4/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation
import AVFoundation

protocol RecorderControllerProtocol {
    
    func startedRecording()
    func finishedRecording(success: Bool)
    
}

class RecorderController: NSObject, AVAudioRecorderDelegate {
    
    static let shared = RecorderController()
    
    var delegate: RecorderControllerProtocol?
    
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    
    func setup() {
        
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        STTController.shared.setup()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
        
    }
    
    func startRecording() {
        
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
//            AVFormatIDKey: Int(kAudioFileWAVEType),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
            
            delegate?.startedRecording()
        } catch {
            finishRecording(success: false)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func getRecordedFilePath() -> URL {
        return getDocumentsDirectory().appendingPathComponent("recording.m4a")
    }
    
    func finishRecording(success: Bool) {
        
        if audioRecorder == nil {
            return
        }

        audioRecorder.stop()
        audioRecorder = nil
        
        delegate?.finishedRecording(success: success)
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
}
