//
//  HomeViewController.swift
//  MusicApp
//
//  Created by Alejandro Castillejo on 7/2/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import UIKit
import Intents
import IntentsUI
import AVFoundation

class HomeViewController: UIViewController {
    
    @IBOutlet weak var recorderButton: UIButton!
    @IBOutlet weak var playerButton: UIButton!
    @IBOutlet weak var siriListenLabel: UILabel!
    
    @IBOutlet weak var addToSiriContainer1: UIView!
    @IBOutlet weak var addToSiriContainer2: UIView!
    @IBOutlet weak var addToSiriContainer3: UIView!
    @IBOutlet weak var addToSiriContainer4: UIView!
    @IBOutlet weak var addToSiriContainer5: UIView!
    @IBOutlet weak var addToSiriContainer6: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Set up recorder
        recorderButton.setTitle("Tap to Record", for: .normal)
        recorderButton.addTarget(self, action: #selector(recordTapped), for: .touchUpInside)
        
        // Set up player
        playerButton.addTarget(self, action: #selector(playTapped), for: .touchUpInside)
        playerButton.isEnabled = false
        playerButton.setTitle("Play", for: .normal)
        
        setupSiriShortcutButtons()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        SiriSimulatorController.shared.setup()
        SiriSimulatorController.shared.delegate = self
    }

    func setupSiriShortcutButtons() {
        
        addSiriButton(to: addToSiriContainer1, activityTitle: "Record", invocationPhrase: "Record this melody")
        addSiriButton(to: addToSiriContainer2, activityTitle: "Start metronome", invocationPhrase: "Start metronome")
//        addSiriButton(to: addToSiriContainer3, activityTitle: "Recognize note", invocationPhrase: "What note is this?")
        addSiriButton(to: addToSiriContainer4, activityTitle: "Tune guitar", invocationPhrase: "Tune guitar")
//        addSiriButton(to: addToSiriContainer5, activityTitle: "Again", invocationPhrase: "Again")
        
//        let intent = ControlMetronomeIntent()
//        intent.bpm = 120
//        let interaction = INInteraction(intent: intent, response: nil)
//
//        // The order identifier is used to match with the donation so the interaction
//        // can be deleted if a soup is removed from the menu.
//        interaction.identifier = "uuidString"
//
//        interaction.donate { (error) in
//            if error != nil {
//                if let error = error as NSError? {
//                    print("Interaction donation failed")
//                    print(error)
//
//                }
//            } else {
//                print("Successfully donated interaction")
//            }
//        }
        
//        let intent = CreateLoopIntent()
////        let c = Chord(identifier: "c", display: "C")
////        let d = Chord(identifier: "d", display: "D")
//        intent.chords = "C, G, F, Am"
//        let interaction = INInteraction(intent: intent, response: nil)
//
//        // The order identifier is used to match with the donation so the interaction
//        // can be deleted if a soup is removed from the menu.
//        interaction.identifier = "uuidString"
//        intent.suggestedInvocationPhrase = "Create loop"
//
//        interaction.donate { (error) in
//            if error != nil {
//                if let error = error as NSError? {
//                    print("Interaction donation failed")
//                    print(error)
//
//                }
//            } else {
//                print("Successfully donated interaction")
//            }
//        }
        
    }
    
    @available(iOS 12.0, *)
    func addSiriButton(to view: UIView, activityTitle: String, invocationPhrase: String) {
        
        let button = INUIAddVoiceShortcutButton(style: .black)
        button.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(button)
        button.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        setupShortcut(to: button, activityTitle: activityTitle, invocationPhrase: invocationPhrase)
        
    }
    
    @available(iOS 12.0, *)
    func setupShortcut(to button: INUIAddVoiceShortcutButton?, activityTitle: String, invocationPhrase: String) {

        let activity = NSUserActivity(activityType: "your_reverse_bundleID.\(activityTitle)")
        activity.userInfo = ["speech": "\(activityTitle)"]
        
        activity.title = activityTitle
        activity.suggestedInvocationPhrase = invocationPhrase
        
        activity.isEligibleForSearch = true
        activity.isEligibleForPrediction = true
        activity.persistentIdentifier = NSUserActivityPersistentIdentifier("your_reverse_bundleID.\(title)")
        view.userActivity = userActivity
        activity.becomeCurrent()
        self.userActivity = activity
        
        button?.shortcut = INShortcut(userActivity: activity)
        button?.delegate = self
        
    }
    
    @IBAction func simulatedGuitarButtonPressed(_ sender: Any) {
        SiriSimulatorController.shared.simulatedGuitarButtonPressed()
    }
    
    @objc func playTapped() {
        
        if (playerButton.titleLabel?.text == "Play"){
            recorderButton.isEnabled = false
            playerButton.setTitle("Stop", for: .normal)
            PlayerController.shared.play()
        } else {
            PlayerController.shared.stop()
            playerButton.setTitle("Play", for: .normal)
        }
    }
    
    @objc func recordTapped() {
        
        if (recorderButton.titleLabel?.text == "Tap to Record"){
            RecorderController.shared.startRecording()
        } else {
            RecorderController.shared.finishRecording(success: true)
        }
        
    }
    
}

@available(iOS 12.0, *)
extension HomeViewController: INUIAddVoiceShortcutViewControllerDelegate {
    func addVoiceShortcutViewController(_ controller: INUIAddVoiceShortcutViewController, didFinishWith voiceShortcut: INVoiceShortcut?, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func addVoiceShortcutViewControllerDidCancel(_ controller: INUIAddVoiceShortcutViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

@available(iOS 12.0, *)
extension HomeViewController: INUIAddVoiceShortcutButtonDelegate {
    
    func present(_ addVoiceShortcutViewController: INUIAddVoiceShortcutViewController, for addVoiceShortcutButton: INUIAddVoiceShortcutButton) {
        addVoiceShortcutViewController.delegate = self
        addVoiceShortcutViewController.modalPresentationStyle = .formSheet
        present(addVoiceShortcutViewController, animated: true, completion: nil)
    }
    
    func present(_ editVoiceShortcutViewController: INUIEditVoiceShortcutViewController, for addVoiceShortcutButton: INUIAddVoiceShortcutButton) {
        editVoiceShortcutViewController.delegate = self
        editVoiceShortcutViewController.modalPresentationStyle = .formSheet
        present(editVoiceShortcutViewController, animated: true, completion: nil)
    }
    
}

@available(iOS 12.0, *)
extension HomeViewController: INUIEditVoiceShortcutViewControllerDelegate {
    
    func editVoiceShortcutViewController(_ controller: INUIEditVoiceShortcutViewController, didUpdate voiceShortcut: INVoiceShortcut?, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func editVoiceShortcutViewController(_ controller: INUIEditVoiceShortcutViewController, didDeleteVoiceShortcutWithIdentifier deletedVoiceShortcutIdentifier: UUID) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func editVoiceShortcutViewControllerDidCancel(_ controller: INUIEditVoiceShortcutViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}

extension HomeViewController: SiriSimulatorControllerProtocol {
    
    func transcriptionUpdated(_ string: String) {
        siriListenLabel.text = string
    }
    
    func transcriptionFinished() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.siriListenLabel.text = ""
        }
    }
    
}
