//
//  StringExtension.swift
//  MusicApp
//
//  Created by Alejandro Castillejo on 7/5/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation

extension String {
    
    func stripFileExtension () -> String {
        var components = self.components(separatedBy: ".")
        guard components.count > 1 else { return self }
        components.removeLast()
        return components.joined(separator: ".")
    }
    
}
