//
//  INPlayMediaIntentHandler.swift
//  IntentExtension
//
//  Created by Alejandro Castillejo on 7/6/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Intents

class INPlayMediaIntentHandler: NSObject, INPlayMediaIntentHandling {
    
    func resolveMediaItems(for intent: INPlayMediaIntent, with completion: @escaping ([INPlayMediaMediaItemResolutionResult]) -> Void) {
        
        print("resolveMediaItems")
        
        if let name = intent.mediaSearch?.mediaName {
            print(name)
            
            let mediaItem = INMediaItem(identifier: "1", title: name, type: .song, artwork: nil)
            
            //            var mediaItem =  INMediaItem(identifier: itemID.uuidString,
            //                                         title: title,
            //                                         type: containerType.mediaItemType,
            //                                         artwork: INImage(named: artworkName))
            
            print("Completion success")
            completion([INPlayMediaMediaItemResolutionResult.success(with: mediaItem)])
        } else {
            
            print("Completion unsupported")
            completion([INPlayMediaMediaItemResolutionResult.unsupported()])
            
        }
        
    }
    
    func handle(intent: INPlayMediaIntent, completion: @escaping (INPlayMediaIntentResponse) -> Void) {
        
        print("handle INPlayMediaIntent")
        
        //        let activity: NSUserActivity = NSUserActivity(activityType: "yo")
        //        completion(INPlayMediaIntentResponse(code: .handleInApp, userActivity: nil))
        
//        guard let mediaTitle = intent.mediaItems?.first?.title else {
//            return
//        }
//
//        if mediaTitle.lowercased() == "e" {
//            let response = INPlayMediaIntentResponse(code: .continueInApp, userActivity: nil)
//            completion(response)
//        } else {
//            let response = INPlayMediaIntentResponse(code: .handleInApp, userActivity: nil)
//            completion(response)
//        }
        
        let activityTitle = "Play note"
        let activity = NSUserActivity(activityType: "your_reverse_bundleID.\(activityTitle)")
        activity.title = activityTitle
        
        let response = INPlayMediaIntentResponse(code: .continueInApp, userActivity: activity)
        completion(response)
        
    }
    
    func confirm(intent: INPlayMediaIntent, completion: @escaping (INPlayMediaIntentResponse) -> Void) {
        print("Confirm")
        
        let response = INPlayMediaIntentResponse(code: .ready, userActivity: nil)
        //        response.nowPlayingInfo = AudioPlaybackManager.nowPlayingInfo(for: firstItem.episode, in: firstItem.container, forShortcut: true)
        completion(response)
        
    }
    
}

