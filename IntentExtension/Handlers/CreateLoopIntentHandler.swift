//
//  CreateLoopsIntentHandler.swift
//  IntentExtension
//
//  Created by Alejandro Castillejo on 7/6/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation
import Intents

class CreateLoopIntentHandler: NSObject, CreateLoopIntentHandling {
    
    func resolveChords(for intent: CreateLoopIntent, with completion: @escaping (INStringResolutionResult) -> Void) {
        
        if let chords = intent.chords {
            //            let bpmInt = Int(truncating: bpm)
            completion(INStringResolutionResult.success(with: String(chords)))
            //            completion(.unsupported(forReason: .greaterThanMaximumValue))
        } else {
            completion(INStringResolutionResult.needsValue())
        }
        
    }

    func handle(intent: CreateLoopIntent, completion: @escaping (CreateLoopIntentResponse) -> Void) {
        
        print("Handle Create loops")
        
        let activityTitle = "Create loop"
        let activity = NSUserActivity(activityType: "your_reverse_bundleID.\(activityTitle)")
        activity.title = activityTitle
        let response = CreateLoopIntentResponse(code: .continueInApp, userActivity: activity)
        //        response.userActivity = userActivity
        completion(response)
        
    }
    
}
