//
//  Test.swift
//  MusicApp
//
//  Created by Alejandro Castillejo on 7/6/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Foundation

class ControlMetronomeIntentHandler: NSObject, ControlMetronomeIntentHandling {
    
    func resolveBpm(for intent: ControlMetronomeIntent, with completion: @escaping (ControlMetronomeBpmResolutionResult) -> Void) {
        print("Resolve Control Metronome, BPM")
        
        print(intent.bpm)
        
        if let bpm = intent.bpm {
            //            let bpmInt = Int(truncating: bpm)
            completion(ControlMetronomeBpmResolutionResult.success(with: Int(truncating: bpm)))
            //            completion(.unsupported(forReason: .greaterThanMaximumValue))
        } else {
            completion(ControlMetronomeBpmResolutionResult.needsValue())
        }
        //        completion(ControlMetronomeBpmResolutionResult.needsValue())
        
    }
    
    func handle(intent: ControlMetronomeIntent, completion: @escaping (ControlMetronomeIntentResponse) -> Void) {
        print("Handle Control Metronome")
        
        //        response = ControlMetronomeIntentResponse.success(orderDetails: orderDetails, soup: intent.soup, waitTime: formattedWaitTime)
        
        let activityTitle = "Control metronome"
        let activity = NSUserActivity(activityType: "your_reverse_bundleID.\(activityTitle)")
        activity.title = activityTitle
        if let bpm = intent.bpm {
            activity.userInfo = ["bpm": Int(truncating: bpm)]
        }
        let response = ControlMetronomeIntentResponse(code: .continueInApp, userActivity: activity)
        //        response.userActivity = userActivity
        completion(response)
        
    }
    
    
}
