//
//  IntentHandler.swift
//  IntentExtension
//
//  Created by Alejandro Castillejo on 6/30/19.
//  Copyright © 2019 Alejandro Castillejo. All rights reserved.
//

import Intents

class IntentHandler: INExtension {
    
    override func handler(for intent: INIntent) -> Any {
        // This is the default implementation.  If you want different objects to handle different intents,
        // you can override this and return the handler you want for that particular intent.
        print("Default handler")
        
        if intent is ControlMetronomeIntent {
            return ControlMetronomeIntentHandler()
        } else if intent is INPlayMediaIntent {
            return INPlayMediaIntentHandler()
        } else if intent is CreateLoopIntent {
            return CreateLoopIntentHandler()
        } else {
            return self
        }

    }
    
}
